<?php

namespace Giift\CurrencyConverter\Providers;

use Http\Client\HttpClient;
use GuzzleHttp\Psr7\Request;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ExchangeRate
 * @package Giift\CurrencyConverter\Providers
 */
class ExchangeRate extends Provider implements IProvider
{
    /**
     * @param string $from
     * @param string $to
     * @return float
     * @throws \Exception
     */
    public function getRate($from, $to)
    {
        $endpoint = "https://v3.exchangerate-api.com/pair/{$this->key()}/$from/$to";
        $request = new Request('GET', $endpoint);
        try {
            $response = $this->httpClient()->sendRequest($request);
            if ($response->getStatusCode() === 200) {
                return $this->parseRateResponse($response, $from, $to);
            }
            throw new \Exception('ExchangeRate - getRate - status code was not 200.');
        } catch (\Exception $e) {
            $this->getLogger()->warning('ExchangeRate - getRate - Failed to get currency. '.$e->getMessage());
        }
        return null;
    }

    /**
     * @param ResponseInterface $response
     * @param $from
     * @param $to
     * @return float
     * @throws \Exception
     */
    private function parseRateResponse(ResponseInterface $response, $from, $to)
    {
        $body = $response->getBody()->getContents();
        $body = json_decode($body, true);

        if (!isset($body['rate'])) {
            $this->getLogger()->warning("{static::class} - parseRateResponse - Failed to get currency for $from -> $to.");
            throw new \Exception("{static::class} - parseRateResponse - Failed to get currency for $from -> $to");
        }

        //Pulls the rate from the body.
        $rate = $body['rate'];

        //Some safety checks.
        if (!is_numeric($rate)) {
            $this->getLogger()->warning("{static::class} - parseRateResponse - Rate is not numeric.");
            throw new \Exception("{static::class} - parseRateResponse - Rate is not numeric.");
        }

        if (!is_float($rate)) {
            $rate = floatval($rate);
        }

        return $rate;
    }

    /**
     * @return string
     */
    private function key()
    {
        return isset($this->config['key']) ? $this->config['key'] : '';
    }
}
