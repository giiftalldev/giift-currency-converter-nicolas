<?php

namespace Giift\CurrencyConverter\Providers;

use Http\Client\HttpClient;
use GuzzleHttp\Psr7\Request;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Fixerio
 * @package Giift\CurrencyConverter\Providers
 */
class Fixerio extends Provider implements IProvider
{
    /**
     * @param $fromCurrency
     * @param $toCurrency
     * @inheritdoc
     */
    public function getRate($fromCurrency, $toCurrency)
    {
        $endpoint = "https://api.fixer.io/latest?base=$fromCurrency&symbols=$toCurrency";
        $request = new Request('GET', $endpoint);
        try {
            $response = $this->httpClient()->sendRequest($request);
            if ($response->getStatusCode() === 200) {
                return $this->parseRateResponse($response, $fromCurrency, $toCurrency);
            }
            throw new \Exception('Fixerio - getRate - status code was not 200.');
        } catch (\Exception $e) {
            $this->getLogger()->warning('Fixerio - getRate - Failed to get currency. '.$e->getMessage());
        }
        return null;
    }

    /**
     * @param ResponseInterface $response
     * @param $from
     * @param $to
     * @return float
     * @throws \Exception
     */
    private function parseRateResponse(ResponseInterface $response, $from, $to)
    {
        $body = $response->getBody()->getContents();
        $body = json_decode($body, true);

        if (!isset($body['rates'][$to])) {
            $this->getLogger()->warning("Fixerio - parseRateResponse - Failed to get currency for $from -> $to.");
            throw new \Exception("Fixerio - parseRateResponse - Failed to get currency for $from -> $to");
        }

        //Pulls the rate from the body.
        $rate = $body['rates'][$to];

        //Some safety checks.
        if (!is_numeric($rate)) {
            $this->getLogger()->warning("Fixerio - parseRateResponse - Rate is not numeric.");
            throw new \Exception("Fixerio - parseRateResponse - Rate is not numeric.");
        }

        if (!is_float($rate)) {
            $rate = floatval($rate);
        }

        return $rate;
    }
}
