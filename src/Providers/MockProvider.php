<?php

namespace Giift\CurrencyConverter\Providers;

use Http\Client\HttpClient;
use Psr\Log\LoggerInterface;

/**
 * Class MockProvider
 * @package Giift\CurrencyConverter\Providers
 */
class MockProvider extends Provider implements IProvider
{
    private $rate = 1.00;

    /**
     * @param $fromCurrency
     * @param $toCurrency
     * @return float
     */
    public function getRate($fromCurrency, $toCurrency)
    {
        //Just a warning so that the Mock Provider is being used.
        $this->getLogger()->warning('Mock Currency Provider being used.');
        return $this->rate;
    }

    /**
     * Set a hard rate.
     * @param $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }
}
